# trading-freedom

Simple cryptocurrency Bittrex trading helper app

By the moment this application is just a simple API that uses information obtained from the Bittrex exchange API to present it in an organized way in Spanish.

The idea is to build also a mobile APP to show you relevant information about the ups and downs of the chosen currencies
